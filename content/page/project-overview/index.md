---
date: 2016-04-23T20:26:34+02:00
menu: "main"
title: Projektöversikt
weight: 10
---

Det finns ingen övergripande plats att gå till där översättning av program och relaterade skrifter översätts, utan det sker i olika gemenskaper och projekt. Det gäller att leta sig fram till var en översättning ska ske ibland. För just fri programvara är en svensk gemensam samlingspunkt sändlistan TP-sv, som varit aktiv sedan 90-talet.


## Projektexempel

Några projekt som samlar en mängd översättningar med ett visst fokus eller tema:

[TP-sv][1]: En sändlista med lång historia. Hanterar framför allt projekt från [Translation Project][2], men vill man ha synpunkter och hjälp med nästan vilken annan översättning som helst kan man vända sig hit. En inofficell samlingspunkt inom rörelsen för översättningar på svenska.

[Transifex][3]: En översättningsplattform som har massor och återigen massor av projekt av olika storlek och dignitet. VLC, och mycket annat hittar du här.

[Weblate][4]: En annan samlingspunkt som har långt ifrån lika många projekt som Transifex.

[GNU Savannah][5]: Här hittar man en del av GNUs projekt.

[Mozilla l10n][6]: Här översätts Firefox och andra Mozilla-projekt, länken går till introduktionswikisidan om hur man går vidare.

[Ubuntu][7]: För att översätta Ubuntu-projektet. Länken går till introduktionssidan om hur man kommer igång. Många av projekten där egentligen översättas längre upp i kedjan, som exempelvis hos GNOME eller Debian.

[Debian][8]: Har en omfattande statistik kring paket som berör Debian-projektet (och därmed många underprojekt som Ubuntu, Linux Mint med mera).

[Linux Mint][9]: För att översätta Linux Mint. Många av översättningarna bör dock ske uppströms.

[GNOME - Förbannade lögner][10]: Skrivbordsmiljön med tillhörande program.

[KDE][11]:Skrivbordsmiljön med tillhörande program.

[Open Suse][12]:Skrivbordsmiljön med tillhörande program.

[LXDE][13]: Skrivsbordsmiljön med tillhörande program

[Enlightenment][14]: Skrivbordsmiljön.

[TDLP][15]: Massor av guider och annat. Trots att mycket inte blivit uppdaterat på länge är en hel del aktuellt fortfarande. 

## Enstaka program

Vill man översätta enstaka program gör man bäst i att leta runt bland sina favoritprogram och se vilka som inte är översatta/dåligt översatta, eller så kan man titta in på exempelvis alternative.to för att finna de vanligaste och ovanligaste programmen i behov att dokumentation. Sedan får man försöka att finna var och hur de översättas, allt ifrån på GitLab till att man skickar en översatt fill till utvecklaren med e-post. Här kommer vi att listalite program vi gillar, och som inte finns under webbplatser som listas under samlingsprojekt.

[1]: http://www.tp-sv.se/
[2]: https://translationproject.org/html/welcome.html
[3]: https://www.transifex.com/
[4]: https://hosted.weblate.org/projects/
[5]: https://savannah.gnu.org/
[6]: https://wiki.mozilla.org/L10n:Home_Page
[7]: https://wiki.ubuntu.com/SwedishTranslation
[8]: https://www.debian.org/international/dutch/index.html
[9]: https://translations.launchpad.net/+groups/linux-mint
[10]: https://l10n.gnome.org/
[11]: https://l10n.kde.org/
[12]: https://i18n.opensuse.org/
[13]: https://wiki.lxde.org/en/How_to_contribute_into_translating_LXDE
[14]: https://launchpad.net/enlightenment
[15]:



