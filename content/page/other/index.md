---
date: 2016-04-23T20:26:34+02:00
menu: "main"
title: Övrigt
weight: 40
---

## Annat 

Här listas annat intressant som inte riktigt passar in under övriga rubriker.

Kom igång att översätta ubuntus wiki. 

Uppsats om översättning av fri programvara

Att skriva bra teknisk dokumentation

B-translator - ett projekt för att låta allmänhet ge synpunkter på översättningar
