---
date: 2017-04-23T15:21:22+02:00
title: Om
type: homepage
menu: main
weight: 5
---

## Fri programvara och öppen källkod på svenska

Den här webbplatsen samlar resurser om FOSS[^1] och Linux på svenska, blandat med en och annan bloggupdatering.

Här hittar du länkar till översatta skrivbordsmiljöer, program,  böcker, historiska resurser och annat närliggande. Allt för att hjälpa dig som är nyfiken på området att snabbt komma igång med att översätta.

## Varför på svenska?

Varför bemöda sig överhuvudtaget - bör man inte använda program och operativsystem på engelska på grund av (välj orsak)? Det är självklart ditt val - själv älskar vi språk, och vi tycker att det svenska språket är såpass vackert i sig att vi vill fortsätta att använda det även med våra favoritsystem. Vi tycker att det vore en mycket tråkigare och färglösare värld om vi bara använda engelska. 

Med det sagt så lämpar sig kanske inte allt att använda på svenska, ett exempel kan vara tunga utvecklarnära verktyg på lågnivå - många ord inom tekniken är redan djupt inarbetade och skulle förlora i förståelse på att tvingas in i en svensk klädsel. Därför försöker vi också vara pragmatiska när vi översätter.

[^1]: FOSS - Free and Open Source Software, ett samlingsnamn för fri programvara och öppen källkod
