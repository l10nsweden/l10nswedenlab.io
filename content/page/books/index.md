---
date: 2016-04-23T20:26:34+02:00
menu: "main"
title: Böcker
weight: 20
---

Det finns många böcker som är släppta under fria licenser. En del är på svenska, några är översatta och många fler är oöversatta. Att översätta en bok är ett större projekt än ett mellanstort program. 

### Böcker om fri programvara och öppen källkod på svenska, översatta eller skrivna på svenska:

[GNU/Linux, Apache och nätverk][2] [pdf][3] [LaTex][4] Finns även att köpa. Licens: GNU FDL, 2013, Andreas Önnebring, HME Publishing

[Att använda GNU/Linux][11] [pdf][10] Fortfarande ganska mycket aktuellt (och mycket inaktuellt). Licens: ej fri licens, men fri att distribuera, 2006, Linus Walleij

[Effektivare Linux][6] [pdf][7] [latex][5] Finns att köpa. Licens: GNU FDL 2010, Tobias Haglund, HME Publishing

## Oöversatta böcker under fria licenser:

[Debian-relaterade böcker][12] Lista över Debianböcker på olika språk. En del översatta.

[Free software, free society][13] R.Stallman, Licens: GFDL

[Little Brother][14] C. Doctorow, Licens: Creative Commons




[2]: http://einstein.df.lth.se/glan/
[3]: https://mega.nz/#!SEpwGCwZ!vbFTNtwjqo1NuM5WeQzOUNDS0PVCRaUQL5Hlrsx7n8M
[4]: https://mega.nz/#!uBpyQQJa!C3stVGUtjIvWol-gdxtjDApUtmLNPu3MgD6o_n4zY4w
[5]: https://mega.nz/#!zYYXCRLB!e9MJj9f43Jkm1z0In8Y2nczicCDGG75oEtEbc1hAjCM
[7]: https://mega.nz/#!uQBSUJBQ!Q7F-XFx30HGv0Jc8lgYvhts9nv36jI0LggcswaW7KL0
[6]: https://dext.se/pages/hemsida-for-effektivare-linux-kom-igang-med-kommandoraden
[10]: https://mega.nz/#!PExDgazR!3zgdvemhuW2DgPTOdNSJNReJDGkCf_86CeDfeHnAqR8
[11]: https://dflund.se/~triad/
[12]: https://www.debian.org/doc/books
[13]: https://www.gnu.org/doc/fsfs3-hardcover.pdf
[14]: https://www.dfri.se/projekt/oversattning-av-cory-doctorows-little-brother/




