---
date: 2017-04-23T20:26:34+02:00
menu: "main"
title: Historia
weight: 25
---

## Idag 

Det finns ett liten men ganska aktiv gemenskap som översätter program idag. Ibland med en lång historia, men oftast med olika perioder av aktivitet och inblandade. För att se dessa, gå till aktiva översättningsprojekt.

## Genom åren

Genom åren har det varit många inblandade i översättandet av fria och öppna program till svenska. Här finns några intressenta länkar, en del endast tillgängliga genom Wayback Machine. De visar en del att mycket arbete som har skett genom åren, och ibland har lett till något annat och ibland bara runnit ut i sanden. 

[swe-doc][1] Översatte främst TDLP (The Linux Documentation Project]. Inaktivt sedan många år.





[1]: https://web.archive.org/web/20180308200854/http://www.rejas.se/swe-doc/index.html
